file_path='/app/file.txt'

#read the contents of a file into a string
data =  File.read "#{file_path}"

data_array = data.split(" ")

#define a variable to hold the max_length and intially set it first element of array
$largest_word = data_array[0] 

def largest_word(data_array)
  raise StandardError.new('file seems to be empty') if data_array[0].nil? || data_array[0].empty?
    data_array.each { |item|
    if $largest_word.length < item.length
      $largest_word = item
    end
    }
    return $largest_word
end

def transpose(largest_word)
  raise StandardError.new('file seems to be empty') if largest_word.nil? || largest_word.empty?
    total_chars = largest_word.length 
    transposed_string = ''
    while total_chars > 0
      total_chars -= 1 
      transposed_string = transposed_string +  largest_word[total_chars]
    end
    return transposed_string
end

puts "largest word is #{largest_word(data_array)}"
puts "transpose word of the largest word is #{transpose($largest_word)}"
