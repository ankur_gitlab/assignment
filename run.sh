if [ ! -x "$(command -v docker)" ]; then
    echo " PLEASE Install docker"
    exit 1
fi

docker build -t coding_test .
docker run -it coding_test ruby /app/ruby/rubysolution.rb
